
window.tryMainCanvas = () => {
    var myCanvasDiv = document.querySelector('.main-canvas');

    var scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera( 10, myCanvasDiv.clientWidth/myCanvasDiv.clientWidth, 0.1, 1000 );
    var renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
    renderer.setSize( myCanvasDiv.clientWidth, myCanvasDiv.clientWidth );
    myCanvasDiv.appendChild( renderer.domElement );

    window.setObject = (object, material) => {
        //console.log(object);
        object.children.forEach(function (child) {
            child.material = material;
        });
        object.position.y = -13;
        object.scale.set(0.01,0.01,0.01);
        scene.add( object );
    };
    function pointLight(obj) {
        var pointLight = new THREE.PointLight( obj.color, 0.6, 50 );
        pointLight.position.set( obj.x, obj.y, obj.z );
        scene.add( pointLight );
        var sphereSize = 1;
        var pointLightHelper = new THREE.PointLightHelper( pointLight, sphereSize );
        scene.add( pointLightHelper );
    }

    // instantiate the loader
    var light = new THREE.AmbientLight( 0x404040, 0.2 ); // soft white light
    scene.add( light );

    var color = 0xCB5458;

    pointLight({color, x:0, y:5, z:-5 });
    pointLight({color, x:0, y:5, z:5 });

    pointLight({color:0xCBCBC3, x:0, y:0, z:10 });

    pointLight({ color, x:0, y:0, z:-10 });

    pointLight({ color, x:10, y:0, z:0 });

    pointLight({ color, x:-10, y:0, z:0 });
    window.loader = new THREE.OBJLoader();
    window.material = new THREE.MeshPhongMaterial({
        shininess: 0,
        color: 0x4072CB,
        side: THREE.DoubleSide
    });
    // load a resource from provided URL synchronously
    loader.load( 'dist/object/shirt/Body_F_FrenchPlacket.obj', function (object) {
        object.name = 'Front';
        setObject(object, material);
    });
    loader.load( 'dist/object/shirt/Body_B_BoxPleats.obj', function (object) {
        object.name = 'Back';
        setObject(object, material);
    });
    loader.load( 'dist/object/shirt/Collar_ButtonDown.obj', function (object) {
        object.name = 'Collar';
        setObject(object, material);
    });
    loader.load( 'dist/object/shirt/Cuffs_1Button_Square.obj', function (object) {
        object.name = 'Cuffs';
        setObject(object, material);
    });
    loader.load( 'dist/object/shirt/Sleeve_Basic_Single.obj', function (object) {
        object.name = 'Sleeve';
        setObject(object, material);
    });

    var controls = new THREE.OrbitControls( camera );
    camera.position.z = 50;

    var animate = function () {
        requestAnimationFrame( animate );
        renderer.render( scene, camera );
    };

    animate();
};