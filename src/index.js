require('./app.scss');
window.$ = require('jquery');
window.THREE = require('three');
window.OBJLoader = require('three/examples/js/loaders/OBJLoader');
window.OrbitControls = require('three/examples/js/controls/OrbitControls');
import Vue from 'vue/dist/vue';
import VueRouter from 'vue-router';
require('./js/tailorCanvas');


/*Component Load*/
import InsetBox from './components/InsetBox.vue';
import Styles from './components/making/Styles.vue';
import Fabrics from './components/making/fabrics.vue';
import Color from './components/making/Color.vue';
import SleevesMenu from './components/makingMaterials/styles/SleevesMenu.vue';
import CollarMenu from './components/makingMaterials/styles/CollarMenu.vue';
import SelectFabric from './components/makingMaterials/SelectFabric.vue';
import SelectColor from './components/makingMaterials/SelectColor.vue';
const Bar = { template: '<div>bar</div>' };
/*End Component Load*/

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        { path: '/', redirect:{name:'Styles'} },
        {
            path: '/inset',
            component: InsetBox,
            name:'InsetBox',
            children: [
                {
                    path: '/styles',
                    component: Styles,
                    name: 'Styles',
                    children: [
                        {
                            path: 'sleeves',
                            component: SleevesMenu,
                            name: 'SleevesMenu'
                        },
                        {
                            path: 'collar',
                            component: CollarMenu,
                            name: 'CollarMenu'
                        }
                    ]
                },
                {
                    path: '/fabrics',
                    component: Fabrics,
                    name: 'fabrics',
                    children: [
                        {
                            path: ':fabricId/:fabsName',
                            component: SelectFabric,
                            name: 'SelectFabric',
                            props: true
                        }
                    ]
                },
                {
                    path: '/color',
                    component: Color,
                    name: 'color',
                    children: [
                        {
                            path: ':colorId/:colName',
                            component: SelectColor,
                            name: 'SelectColor',
                            props: true
                        }
                    ]
                }

            ] }
    ]
});


var app = new Vue({
    el: '#app',
    data: {
        //..
    },
    methods:{
        //..
    },
    mounted(){
        tryMainCanvas();
    },
    updated(){
        //..
    },
    router
});



